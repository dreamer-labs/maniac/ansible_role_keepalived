ansible_role_keepalived
=======================

This role sets up keepalived with a simple health check

Requirements
------------

No requirements

Role Variables
--------------

```
# The keepalived vip address
keepalived_vip_address: x.x.x.x
# The node to be designated as the initial keepalived master
keepalived_master: MASTER # set node to be master or slave
# The keepalived process ID
keepalived_process_id: haproxy_DH
# The interface which keepalived listens on
keepalived_vrrp_interface
keepalived_vrrp_scripts:
  - name: check_nginx
    script: "killall -0 httpd"
    check_interval: 2
    weight: 2
    user: root
keepalivd_virtual_router_id: 53
keepalived_priority: 101
keepalived_packages:
  redhat:
    - keepalived
    - psmisc
```

Dependencies
------------

```
roles:
  - ansible-role-packages
```

Example Playbook
----------------

```
    - hosts: haproxy
      roles:
         -  role: ansible_role_keepalived
```

License
-------

BSD

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
