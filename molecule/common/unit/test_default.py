import itertools
import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_keepalived_config_file(host):
    f = host.file('/etc/keepalived/keepalived.conf')
    assert f.exists


def test_keepalived_running(host):
    keepalived_svc = host.service("keepalived")
    keepalived_svc.is_running


def test_keepalived_vip(ansible_vars):
    vip = ansible_vars['keepalived_vip_address']
    vrrp_interface = ansible_vars['keepalived_vrrp_interface']

    host_interface_addresses = []

    for hostname in testinfra_hosts:
        host = testinfra.utils.ansible_runner.AnsibleRunner(
            os.environ['MOLECULE_INVENTORY_FILE']
        ).get_host(hostname)

        host_interface_addresses.append(
            host.interface(vrrp_interface).addresses
        )

    assert vip in list(itertools.chain.from_iterable(host_interface_addresses))
